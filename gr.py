import hexchat
import networkx as nx
from networkx.drawing.nx_pydot import write_dot

__module_name__ = "IRC Graph"
__module_version__ = "0.0.9"
__module_description__ = "Graph your IRC network"

ctx = hexchat.find_context(channel="#test")
g = nx.Graph()

def privmsgd(word, word_eol, userdata=None):
    for u in ctx.get_list("users"):
        if (u.nick in word[3:]):
            g.add_edge(word[0].split("!")[0].replace(":", ""), u.nick)
            write_dot(g, "irc.dot")
    return hexchat.EAT_NONE

hexchat.hook_server("PRIVMSG", privmsgd)
