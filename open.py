import networkx as nx
from networkx.drawing.nx_pydot import read_dot
import matplotlib.pyplot as plt

g = read_dot("irc.dot")
plt.subplot()
nx.draw(g, with_label=True, font_weight='bold')
plt.show()
