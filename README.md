This hexchat plugin graphs your IRC network.

Requirements:
1. networkx
2. pydot
3. matplotlib
4. hexchat or xchat

Using on an xchat:
1. sed -i 's/hex/x/g' *.py

Loading the module:
1. Vim edit gr.py, change "#test" to your channel.
2. On hexchat, /py load gr.py

Run the graph:
1. At your console, with $DISPLAY predefined, python open.py

Compatibility with robots:
1. With a few subtle modifications the script can be executed by a robot.
